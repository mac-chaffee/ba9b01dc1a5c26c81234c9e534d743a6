# https://github.com/Rapptz/discord.py
# pip install discord.py
import discord
from discord.ext import commands
# https://github.com/dead-beef/markovchain
# pip install markovchain
from markovchain.text import MarkovText, ReplyMode


bot = commands.Bot(command_prefix='!')
with open("mac-markov.json", "r") as f:
    markov = MarkovText.from_file(f)


@bot.listen()
async def on_message(message):
    if message.author.display_name.startswith("Mac") and not message.content.startswith("!"):
        markov.data(message.content, dataset="mac")
        with open("mac-markov.json", "w") as f:
            markov.save(f)

            
@bot.command()
async def what_would_mac_say(context):
    text = markov(20)
    if text:
        await context.send(text)
    else:
        await context.send("I'm still learning...")


bot.run("Your bot's secret token")